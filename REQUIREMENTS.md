# Make sure you have the following software installed
* GIT (https://git-scm.com/)
* NODE & NPM (https://nodejs.org/en/download/)
  * (windows) RUN ```npm install --global windows-build-tools``` after NPM has been installed( https://www.npmjs.com/package/windows-build-tools ) 
* MetaMask (https://metamask.io/)
* An IDE and command line tool. We will use VSCode and git bash (git bash is installed with GIT).
  * VSCode (https://code.visualstudio.com/)
  * VSCode - solidity extension (https://marketplace.visualstudio.com/items?itemName=JuanBlanco.solidity)

# Clone project
* ```git clone -b course-001 https://gitlab.com/blockchangers/crypto-kitti-app.git && cd crypto-kitti-app && npm install```