pragma solidity >=0.4.21 <0.6.0;

import "./../node_modules/openzeppelin-solidity/contracts/token/ERC721/ERC721.sol";

contract CryptoKitti is ERC721 {
    
    /*   
    Variabler 
     */

    // Definere data til en katt
    struct Kitti {
        string name;
        uint age;
        string gene;
    }

    // Definere en liste med katter
    Kitti[] kittis;

    // Definere totalt antall katter
    uint public kittiCount;

    /*
     Constructor 
     */
    constructor() public {
        // Vi ønsker å starte med å lage en katt så vi vet det fungerer
        kittiCount = 0;
        createKitti("Genesis kitti", 0, "ABCD");
    }

    /* 
    Functions
     */

    // lage en katt
    function createKitti(string memory name, uint age, string memory gene) public {
        // Lag katten og behold den i Memeory.
        Kitti memory kitti = Kitti(name, age, gene);
        // Legg katten inn i listen og lag listereferansen.
        uint tokenId = kittis.push(kitti) - 1;
        // Bruk ERC-721 til å koble dataen til en token som har mange funksjoner.
        _mint(msg.sender,tokenId);
        // Inkrementer counter så vi vet hvor mange katter vi skal hente ut til listen vår.
        kittiCount += 1;
    }

    // hente en katt fra kontrakten
    function getKitti(uint tokenId) public view returns(uint id, address owner, string memory name, uint age, string memory gene) {
        // Hent ut katten og behold den i memory
        Kitti memory kitti = kittis[tokenId];
        // Return variablene i riktig rekkefølge. ownerOf henter vi fra ERC721
        return(tokenId, ownerOf(tokenId), kitti.name, kitti.age, kitti.gene);
    }

    /* 
    Modifiers
     */
}
