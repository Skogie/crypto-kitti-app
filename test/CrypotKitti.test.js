/* eslint-disable no-undef */
// eslint-disable-next-line no-undef
let CryptoKitti = artifacts.require('./CryptoKitti.sol')

// eslint-disable-next-line no-undef
contract('RoleService', async accounts => {
    let owner = accounts[0]
    let user2 = accounts[1]


    it("Deploy should run smooth", async () => {
        const contract = await CryptoKitti.deployed()
        const genesisKitti = await contract.getKitti(0)
        assert.equal(
            "Genesis kitti",
            genesisKitti.name,
            'Verify that the first kitty is "Genesis kitti"'
        )
        assert.equal(
            "AABB",
            genesisKitti.gene,
            'Verify that gene inputed from solidity is the same as from test'
        )
    })

    it("User should be able to create kitti", async () => {
        const contract = await CryptoKitti.deployed()
        const kittiName = "Test kitti"
        await contract.createKitti(kittiName, 2, "BBCC")
        const testKitti = await contract.getKitti(1)
        assert.equal(
            kittiName,
            testKitti.name,
            'Verify that the test kitty is named ' + testKitti
        )

    })



})

