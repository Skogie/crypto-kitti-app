import Vue from 'vue'
import Vuex from 'vuex'
Vue.use(Vuex)
import Web3 from "web3"

let web3 = window.web3

if (typeof web3 !== "undefined") {
  console.log("Useing injected web3 provider")
  web3 = new Web3(web3.currentProvider)
}

import * as cryptoKittiJSON from "./../build/contracts/CryptoKitti.json"
import { resolve } from 'path';

let getKittiInterval
let cryptoKittiContract
// let cryptoKittiContract = new web3.eth.Contract(
//   cryptoKittiJSON.abi,
//   cryptoKittiJSON.networks[Object.keys(cryptoKittiJSON.networks)[0]].address
// )


export default new Vuex.Store({
  state: {
    coinbase: "",
    network: "",
    balance: "",
    txCount: "",
    block: null,
    kittis: {},
    crypotKittiAddress: "",
  },
  getters: {
    coinbase: state => state.coinbase,
    network: state => state.network,
    balance: state => state.balance,
    txCount: state => state.txCount,
    block: state => state.block,
    kittis: state => state.kittis,
    hasKittis: state => Object.keys(state.kittis).length !== 0
  },
  actions: {
    async getKitti({ commit, state }, payload) {
      try {
        let kitti = await cryptoKittiContract.methods.getKitti(payload.id).call()
        // console.log("Got kitti " + kitti.name, kitti)
        commit('kitti', kitti)
        return kitti
      } catch (error) {
        console.log("Could not get kitti. Payload was: ", payload, " + error was: ", error)
      }
    },
    async getAllKittis({ dispatch }) {
      const kittiCount = await dispatch('getKittiCount');
      for (let i = 0; i < kittiCount; i++) {
        try {
          dispatch('getKitti', { id: i })
        } catch (error) {
          console.log("Stopped getting kittis because could not find anymore")
          break
        }
      }
    },
    async getKittiCount({ state }) {
      try {
        let kittiCount = await cryptoKittiContract.methods.kittiCount().call()
        console.log("Got kittiCount ", kittiCount)
        return kittiCount
      } catch (error) {
        console.log("Could not get kittiCount", error)
      }
    },
    async createKitti({ commit, state, dispatch }, payload) {
      try {
        payload = {
          ...{
            name: "Did not get any name",
            age: 0,
            gene: randomString()
          },
          ...payload
        }
        cryptoKittiContract.methods.createKitti(payload.name, payload.age, payload.gene).send({ from: state.coinbase })
          .then(receipt => {
            console.log("receipt", receipt)
          })
      } catch (error) {
        console.log("Could not create kitti. Payload was: ", payload, " + error was: ", error)
      }
    },
    async init({ dispatch, commit }) {
      await dispatch('changeContractAddress')
      commit('network', await dispatch('network'))
      commit('coinbase', await dispatch('coinbase'))
      commit('balance', await dispatch('balance'))
      commit('txCount', await dispatch('txCount'))
      commit('block', await dispatch('block'))
      commit('crypotKittiAddress', await dispatch('crypotKittiAddress'))
      dispatch('getAllKittis')
      getKittiInterval = setInterval(() => {
        dispatch('getAllKittis')
      }, 2000);
    },
    async crypotKittiAddress({ dispatch, commit }, payload) {
      try {
        payload = {
          ...{ network: await dispatch('network') },
          ...payload,
        }
        const address = cryptoKittiJSON.networks[payload.network].address
        commit('crypotKittiAddress', address)
        return address
      } catch (error) {
        console.log("Could not get crypotKittiAddress. Payload was: ", payload, " + error was: ", error)
      }
    },
    async changeContractAddress({ dispatch, commit }, payload) {
      try {
        return new Promise(async (resolve) => {
          payload = {
            ...{ address: await dispatch('crypotKittiAddress') },
            ...payload,
          }
          console.log("payload adddress", payload.address)
          let contract = new web3.eth.Contract(
            cryptoKittiJSON.abi,
            payload.address
          )
          commit('cryptoKittiContract', { contract: contract })
          setTimeout(() => {
            dispatch('getAllKittis')
          }, 1000);
          resolve()
        })
      } catch (error) {
        console.log("Could not change contract address. Payload was: ", payload, " + error was: ", error)
      }
    },
    async network() {
      try {
        return await web3.eth.net.getId()
      } catch (error) {
        console.log("Unable to retrieve network ID. Error: ", error)
      }
    },
    async coinbase() {
      try {
        return await web3.eth.getCoinbase()
      } catch (error) {
        console.log("Unable to retrieve coinbase. Error: ", error)
      }
    },
    async balance({ dispatch }, payload) {
      try {
        if (payload === undefined) {
          payload = {
            ...{ address: await dispatch("coinbase") },
            payload
          }
        }
        return await web3.eth.getBalance(payload.address)
      } catch (error) {
        console.log("Unable to retrieve balance. Error: ", error)
      }
    },
    async txCount({ dispatch }, payload) {
      try {
        if (payload === undefined) {
          payload = {
            ...{ address: await dispatch("coinbase") },
            payload
          }
        }
        return await web3.eth.getTransactionCount(payload.address)
      } catch (error) {
        console.log("Unable to retrieve transactionCount. Error: ", error)
      }
    },
    async block({ }, payload) {
      try {
        if (payload === undefined) {
          payload = {
            ...{ blockNumber: 'latest' },
            payload
          }
        }
        return await web3.eth.getBlock(payload.blockNumber)
      } catch (error) {
        console.log("Unable to retrieve transactionCount. Error: ", error)
      }
    },
  },
  mutations: {
    coinbase(state, payload) {
      state.coinbase = payload;
    },
    network(state, payload) {
      state.network = payload;
    },
    balance(state, payload) {
      state.balance = payload;
    },
    txCount(state, payload) {
      state.txCount = payload;
    },
    block(state, payload) {
      state.block = payload;
    },
    kitti(state, payload) {
      console.log(payload)
      Vue.set(state.kittis, payload.id, payload)
    },
    crypotKittiAddress(state, payload) {
      state.crypotKittiAddress = payload;
    },
    cryptoKittiContract(state, payload) {
      cryptoKittiContract = payload.contract
    },
  },
})

function randomString() {
  var chars = "ABCDE";
  var string_length = 4;
  var randomstring = '';
  for (var i = 0; i < string_length; i++) {
    var rnum = Math.floor(Math.random() * chars.length);
    randomstring += chars.substring(rnum, rnum + 1);
  }
  return randomstring;
}